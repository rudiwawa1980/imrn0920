// Soal No. 1 (Array to Object)

function arrayToObject(arr) {
    var result
    var now = new Date()
    var thisYear = now.getFullYear()
    if (arr.length == 0) console.log('""')
    for (i = 0; i < arr.length; i++) {
        result = i + 1 + '. ' + arr[i][0] + ' ' + arr[i][1] + ':' + '{' +
            'firstName:' + '"' + arr[i][0] + '"' + ',' +
            'lastName:' + '"' + arr[i][1] + '"' + ',' +
            'gender:' + '"' + arr[i][2] + '"' + ',' +
            'age:' + (arr[i][3] && arr[i][3] <= thisYear ? thisYear - arr[i][3] : '"Invalid Birth Year"')
            + '}'
        console.log(result)
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
arrayToObject([]) // ""

console.log("\n")


// Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
    var data = [
        ['Sepatu Stacattu', 1500000],
        ['Baju Zoro', 500000],
        ['Baju H&N', 250000],
        ['Sweater Uniklooh ', 175000],
        ['Casing Handphone  ', 50000],
    ]

    if (!memberId) return "Mohon maaf, toko X hanya berlaku untuk member saja"
    if (memberId && money < 50000) return "Mohon maaf, uang tidak cukup"

    data.sort(function (value1, value2) { return value2[1] - value1[1] })
    var uangSisa = money
    var barangYangDibeli = []
    for (var i = 0; i < data.length; i++) {
        var barang = data[i];
        if (uangSisa >= barang[1]) {
            barangYangDibeli.push(barang[0])
            uangSisa -= barang[1]
        }

    }
    var invoice = {
        memberId: memberId,
        money: money,
        listPurchased: barangYangDibeli,
        changeMoney: uangSisa
    }
    return invoice

}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("\n\n")



// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rutes = ['A', 'B', 'C', 'D', 'E', 'F'];
    result = []

    for (var p = 0; p < arrPenumpang.length; p++) {
        var element = arrPenumpang[p];
        // console.log("naikAngkot -> element", element)
        var penumpang = element[0]
        var naikDari = element[1]
        var tujuan = element[2]
        var bayar = 0
        var invoice = []

        for (var i = 0; i < rutes.length; i++) {
            var dtAwal = rutes[i];
            // console.log("naikAngkot -> dtAwal", dtAwal)
            if (dtAwal == naikDari) {
                var nextRute = i + 1
                for (var j = nextRute; j < rutes.length; j++) {
                    var dtTujuan = rutes[j];
                    bayar += 2000
                    if (dtTujuan == tujuan) break
                }
            }
        }
        invoice = { penumpang: penumpang, naikDari: naikDari, tujuan: tujuan, bayar: bayar }
        result.push(invoice)
    }

    return result

}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]