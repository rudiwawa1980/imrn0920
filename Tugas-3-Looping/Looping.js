//soal 1
console.log("LOOPING PERTAMA")
var counter = 2
while (counter <= 20) {
  console.log(counter + " - " + "I love coding")
  counter += 2
}
console.log("LOOPING PERTAMA")
var counter2 = 20
while (counter2 >= 2) {
  console.log(counter2 + " - " + "I will become a mobile developer")
  counter2 -= 2
}
console.log("\n")

//soal2
var str = ""
for (var counter = 1; counter <= 20; counter++) {
  if (counter % 3 == 0 && counter % 2 != 0) str = "I Love Coding"
  else if (counter % 2 != 0) str = "santai"
  else if (counter % 2 == 0) str = "Berkualitas"
  console.log(counter + " - " + str)
}
console.log("\n")

//soal3
var panjang = 8
var lebar = 4
for (var l = 1; l <= lebar; l++) {
  var pagar = ""
  for (var p = 1; p <= panjang; p++) {
    pagar += "#"
  }
  console.log(pagar + "\n")
}
console.log("\n")

//soal4
var tinggi = 7
var alas = 7
var tmp = 1
for (var l = 1; l <= tinggi; l++) {
  var pagar = ""
  for (var p = 1; p <= tmp; p++) {
    pagar += "#"
  }
  tmp++
  console.log(pagar + "\n")
}
console.log("\n")


//soal5
var panjang = 8
var lebar = 8
for (var l = 1; l <= lebar; l++) {
  var pagar = ""
  for (var p = 1; p <= panjang; p++) {
    if (l % 2 == 0) {
      if (p % 2 == 0) pagar += " "
      else pagar += "+"
    } else {
      if (p % 2 != 0) pagar += " "
      else pagar += "+"
    }

  }
  console.log(pagar + "\n")
}
console.log("\n")