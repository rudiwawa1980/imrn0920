// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let time = 10000
let i = 0
function run() {
    readBooks(time, books[i], (sisaWaktu) => {
        time = sisaWaktu
        i++
        if (i < books.length) run()
    })
}
run()


// Soal No. 2 (Promise Baca Buku)