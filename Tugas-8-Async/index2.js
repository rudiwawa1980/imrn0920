var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let time = 10000
let i = 0
function run() {
    readBooksPromise(time, books[i])
        .then(sisaWaktu => {
            time = sisaWaktu
            i++
            if (i < books.length) run()
        })
        .catch()
}
run()
// Lanjutkan code untuk menjalankan function readBooksPromise