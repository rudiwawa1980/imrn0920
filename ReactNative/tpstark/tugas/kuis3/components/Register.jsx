import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput, Button
} from 'react-native';
// import moduleName from './../images/logo.svg'
import { FontAwesome } from '@expo/vector-icons';
export default class App extends React.Component {
    onPressLearnMore() {
        console.log("App -> onPressLearnMore -> onPressLearnMore")
    }
    render() {
        return (
            <View style={style.container}>
                <Text style={{ fontSize: 30, marginTop: 120 }}>Welcome</Text>
                <Text style={{ fontSize: 12, color: '#4D4D4D' }}>Sign up to continue</Text>
                <View style={{
                    borderWidth: 1,
                    marginTop: 32,
                    paddingBottom:42,
                    paddingTop: 8,
                    borderColor: "rgba(113, 123, 142, 0.2)",
                    borderRadius: 8,


                }}>

                    <View style={{
                        padding: 24,
                    }}>
                        <View style={{ flexDirection: 'column', marginTop: 24, }}>

                            <Text style={{ fontSize: 12, color: '#4D4D4D' }}>Name</Text>
                            <TextInput
                                style={style.input}
                                value="Shakibul islam"
                            />

                            <Text style={{ fontSize: 12, color: '#4D4D4D' }}>Email</Text>
                            <TextInput
                                style={style.input}
                                value="shakibulislam402@gmail.com"
                            />
                            <Text style={{ fontSize: 12, color: '#4D4D4D' }}>Phone number</Text>
                            <TextInput
                                style={style.input}
                                value="+44 213 032 578"
                            />
                            <Text style={{ fontSize: 12, color: '#4D4D4D' }}>Password</Text>
                            <TextInput
                                style={style.input}
                                value="* * * * * * * * * * * * "
                            />



                        </View>
                    </View>
                    <View style={style.buttonSubmitView} >
                        <TouchableOpacity style={style.touchButtonSubmit}>
                            <Button
                                title="Sign Up"
                                color="#F77866"
                                onPress={() => this.onPressLearnMore()}//uji coba fungsi button
                                accessibilityLabel="Learn more about this purple button"
                            />
                        </TouchableOpacity>

                    </View>
                    <View style={{ justifyContent: "center", flexDirection: "row" }}>
                        <Text style={{ fontSize: 12, color: '#4D4D4D', marginTop: 20 }}>Already have an account? </Text>
                        <Text style={{ fontSize: 12, color: '#4D4D4D', marginTop: 20, fontWeight: "bold", color: "#F77866" }}>Sign In </Text>
                    </View>

                </View>




            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        padding: 16,
        // backgroundColor: 'gray',
        flex: 1,
        flexDirection: 'column',
        // flexDirection: 'row',
    },
    textInput: {
        height: 40, borderColor: 'gray', borderWidth: 1, paddingHorizontal: 8,
        marginBottom: 16,
        width: 273,
    },
    buttonSubmitView: {
        marginTop: 32,
        // width: 143,
        // flex: 1,

        flexDirection: 'row-reverse',
    },
    touchButtonSubmit: {
        width: '90%',
        marginHorizontal: '5%',
        borderRadius: 8,

    },
    input: {
        marginBottom: 16
    }
})
