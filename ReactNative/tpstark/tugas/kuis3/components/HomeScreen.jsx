import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    TextInput, Button
} from 'react-native';
// import moduleName from './../images/logo.svg'
import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { EvilIcons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import bajus from './baju.json'
// import moduleName from '../images/'
export const App = (params) => {

    let contentBajuFlashSale
    contentBajuFlashSale = bajus.items.map(baju => (
        <BajuFlashSale name={baju.name} harga={baju.harga} key={baju.id} />
    ))
    let contentMobilFlashSale
    contentMobilFlashSale = bajus.items.map(baju => (
        <MobilFlashSale name={baju.name} harga={baju.harga} key={baju.id} />
    ))



    return (
        <View className={style.container} >

            <ScrollView>
                <View style={{
                    marginTop: 56,
                    borderWidth: 1,
                    paddingHorizontal: 16,
                    marginHorizontal: 16,
                    paddingVertical: 8,
                    borderColor: "rgba(113, 123, 142, 0.2)",
                    borderRadius: 8,
                    flexDirection: "row",
                    justifyContent: "space-between"
                    // backgroundColor:'red'
                }}>
                    <View style={{ flexDirection: "row" }}>
                        <Feather name="search" size={24} color="#727C8E" />
                        <Text style={{ marginLeft: 24, color: "#848991" }}>Search Product</Text>
                    </View>

                    <View style={{ flexDirection: "row-reverse" }}>
                        <EvilIcons name="camera" size={24} color="#727C8E" />
                    </View>


                </View>
                <Image source={require('../images/sanber.png')} style={{ marginTop: 16 }} />
                <View style={{ marginTop: 16, flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 16 }}>
                    <View style={{ alignItems: "center" }}>
                        <FontAwesome name="product-hunt" size={52} color="#FF7171" />
                        <Text style={style.textBarang}>Man</Text>
                    </View>
                    <View style={{ alignItems: "center" }}>
                        <FontAwesome name="product-hunt" size={52} color="#A2F0E6" />
                        <Text style={style.textBarang}>Woman</Text>
                    </View>
                    <View style={{ alignItems: "center" }}>
                        <FontAwesome name="product-hunt" size={52} color="#7AD0FF" />
                        <Text style={style.textBarang}>Kids</Text>
                    </View>
                    <View style={{ alignItems: "center" }}>
                        <FontAwesome name="product-hunt" size={52} color="#CCAAFF" />
                        <Text style={style.textBarang}>Home</Text>
                    </View>
                    <View style={{ alignItems: "center" }}>
                        <FontAwesome name="product-hunt" size={52} color="#9D9E9F" />
                        <Text style={style.textBarang}>More</Text>
                    </View>

                </View>
                <View style={{ marginTop: 24, padding: 16 }}>
                    <Text style={{ fontSize: 24, fontWeight: "bold" }}>Flash Sell</Text>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        style={{
                            flexDirection: "row", marginTop: 16,
                        }}>
                        {contentBajuFlashSale}
                    </ScrollView>
                </View>

                <View style={{ marginTop: 24, padding: 16 }}>


                    <Text style={{ fontSize: 24, fontWeight: "bold" }}>Mobil Flash Sell</Text>

                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        style={{ flexDirection: "row", marginTop: 16 }}
                    >

                        {contentMobilFlashSale}
                    </ScrollView>

                </View>
            </ScrollView>
        </View>
    )
}



export default App


export const BajuFlashSale = (props) => {
    return (
        <View style={{ alignItems: "center" }}>
            <AntDesign name="skin" size={100} color={getRandomColor()} />
            <Text style={style.textBarang}>{props.name}</Text>
            <Text style={{ fontWeight: "bold" }}>$ {props.harga}</Text>
        </View>
    )
}
export const MobilFlashSale = (props) => {
    return (
        <View style={{ alignItems: "center", marginHorizontal: 8 }}>
            <AntDesign name="car" size={150} color={getRandomColor()} />
            <Text style={style.textBarang}>{props.name}</Text>
            <Text style={{ fontWeight: "bold" }}>$ {props.harga}</Text>
        </View>
    )
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

const style = StyleSheet.create({
    container: {
        padding: 16,
        // backgroundColor: 'gray',
        flex: 1,
        // flexDirection: 'column',
        // flexDirection: 'row',
    },
    textInput: {
        height: 40, borderColor: 'gray', borderWidth: 1, paddingHorizontal: 8,
        marginBottom: 16,
        width: 273,
    },
    buttonSubmitView: {
        marginTop: 50,
        // width: 143,
        // flex: 1,

        flexDirection: 'row-reverse',
    },
    touchButtonSubmit: {
        width: '90%',
        marginHorizontal: '5%',
        borderRadius: 8,

    },
    textBarang: {
        color: "#616D80",
        fontSize: 12
    }
})
