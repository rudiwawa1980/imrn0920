import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput, Button
} from 'react-native';
// import moduleName from './../images/logo.svg'
import { FontAwesome } from '@expo/vector-icons';
export default class App extends React.Component {
    onPressLearnMore() {
        console.log("App -> onPressLearnMore -> onPressLearnMore")
    }
    render() {
        return (
            <View style={style.container}>
                <Text style={{ fontSize: 30, marginTop: 120 }}>Welcome Back</Text>
                <Text style={{ fontSize: 12, color: '#4D4D4D' }}>Sign in to continue</Text>
                <View style={{
                    borderWidth: 1,
                    marginTop: 32,
                    paddingVertical: 8,
                    borderColor: "rgba(113, 123, 142, 0.2)",
                    borderRadius: 8,


                }}>

                    <View style={{
                        padding: 24,
                    }}>
                        <View style={{ flexDirection: 'column', marginTop: 52, }}>
                            <Text style={{ fontSize: 12, color: '#4D4D4D' }}>Email</Text>
                            <TextInput
                                style={{}}
                                value="shakibulislam402@gmail.com"
                            />
                            <Text style={{ fontSize: 12, color: '#4D4D4D', marginTop: 32 }}>Email</Text>

                            <TextInput
                                style={{}}
                                value="Password"
                            />

                            <View style={{ flexDirection: "row-reverse" }}>
                                <Text style={{ fontSize: 12, color: '#4D4D4D', marginTop: 20 }}>Forgot Password</Text>
                            </View>

                        </View>
                    </View>
                    <View style={style.buttonSubmitView} >
                        <TouchableOpacity style={style.touchButtonSubmit}>
                            <Button
                                title="Login"
                                color="#F77866"
                                onPress={() => this.onPressLearnMore()}//uji coba fungsi button
                                accessibilityLabel="Learn more about this purple button"
                            />
                        </TouchableOpacity>

                    </View>

                    <View style={{ alignItems: "center", paddingTop: 32 }}>
                        <Text style={{ fontSize: 15, color: '#4C475A', marginTop: 20 }}> - OR - </Text>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: "space-between", marginTop: 32 }}>
                        <View style={{
                            paddingHorizontal: 32, flexDirection: 'row', justifyContent: "flex-start",
                            borderWidth: 1,
                            marginLeft: 16,
                            paddingVertical: 8,
                            borderColor: "#E6EAEE",
                            borderRadius: 6,
                        }}>
                            <FontAwesome name="facebook-official" size={24} color='#3B5998' />
                            <Text style={{ fontSize: 15, color: 'black', marginLeft: 8 }}> Facebook</Text>
                        </View>
                        <View style={{
                            paddingHorizontal: 32, flexDirection: 'row', justifyContent: "flex-start",
                            borderWidth: 1,
                            paddingVertical: 8,
                            borderColor: "#E6EAEE",
                            borderRadius: 6,
                            marginRight: 16,
                        }}>
                            <FontAwesome name="google" size={24} color="gray" />
                            <Text style={{ fontSize: 15, color: 'black', marginLeft: 8 }}> Google</Text>
                        </View>
                    </View>
                </View>




            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        padding: 16,
        // backgroundColor: 'gray',
        flex: 1,
        flexDirection: 'column',
        // flexDirection: 'row',
    },
    textInput: {
        height: 40, borderColor: 'gray', borderWidth: 1, paddingHorizontal: 8,
        marginBottom: 16,
        width: 273,
    },
    buttonSubmitView: {
        marginTop: 50,
        // width: 143,
        // flex: 1,

        flexDirection: 'row-reverse',
    },
    touchButtonSubmit: {
        width: '90%',
        marginHorizontal: '5%',
        borderRadius: 8,

    },
})
