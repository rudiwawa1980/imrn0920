import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Button } from "react-native";
// Buatlah sebuah folder “TugasNavigation” di dalam folder “Tugas” masukkan LoginScreen.js, AboutScreen.js, dan SkillScreen.js.
// import AboutScreen from '../../tugas13/Components/AboutScreen'
// import SkillScreen from '../../tugas14/components/SkillScreen'
// import LoginScreen from '../../tugas13/Components/LoginScreen'
import Login from './components/Login'
import HomeScreen from './components/HomeScreen'
import Register from './components/Register'
import Splash from './components/Splash'
// import { ProjectScreen } from "./ProjectScreen";
// import { AddScreen } from "./AddScreen";



const Drawer = createDrawerNavigator();

export const DrawerStackScreen = (params) => {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Login" component={Login} />
            <Drawer.Screen name="Home" component={TabsScreen} />
        </Drawer.Navigator>
    )
}
const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Home" component={HomeScreen} />
        <Tabs.Screen name="Login" component={Login} />
        <Tabs.Screen name="Register" component={Register} />
        <Tabs.Screen name="Splash" component={Splash} />
    </Tabs.Navigator>
);

const Stack = createStackNavigator();
const RootStackNavigation = ({ userToken }) => (
    <Stack.Navigator headerMode="none">
        {userToken ? (
            <Stack.Screen name="Home" component={DrawerStackScreen} />
        ) : (
                <Stack.Screen name="Login" component={Login} />
            )}
    </Stack.Navigator>
)

export const App = (params) => {
    let userToken = true
    return (
        <NavigationContainer>
            <RootStackNavigation userToken={userToken} />

        </NavigationContainer>

    )
}

export default App