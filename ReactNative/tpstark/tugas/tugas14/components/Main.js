import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import React, { useState, useEffect } from 'react'

import Note from './Note';

export const Main = () => {
  const [noteArray, setNoteArray] = useState([]);
  const [noteText, setNoteText] = useState('');

  let notes = noteArray.map((val, key) => {
    return <Note key={key} keyval={key} val={val} deleteMethod={() => deleteNote(key)} />
  })
  const addNote = () => {
    if (noteText) {
      // alert(noteText)
      let d = new Date()
      setNoteArray([...noteArray, {
        'date': `${d.getFullYear()} / ${d.getMonth() + 1} / ${d.getDate()}`,
        'note': noteText
      }])// sama dengan array push
      setNoteText(noteText)
    }
  }

  const deleteNote = (key) => {
    var array = [...noteArray];
    array.splice(key, 1)
    
    //uji coba memahami slice
    // console.log("deleteNote -> key", key)
    // console.log("deleteNote -> array", array.splice(key, 1))
    // console.log("deleteNote -> array2", array)

    setNoteArray(array)
    // console.log("deleteNote -> deleteNote", noteArray.splice(key, 1))

  }


  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>--Noter--</Text>
      </View>

      <ScrollView style={styles.scrollContainer}>
        {notes}
      </ScrollView>

      <View style={styles.footer}>
        <TextInput
          style={styles.textInput}
          onChangeText={(noteText) => setNoteText(noteText)}
          value={noteText}
          placeholder='>note'
          placeholderTextColor='white'
          underlineColorAndroid='transparent'
        />
      </View>

      <TouchableOpacity onPress={addNote.bind(this)} style={styles.addButton}>
        <Text style={styles.addButtonText}>+</Text>
      </TouchableOpacity>
    </View>
  )
}



export default Main


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#E91E63',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 10,
    borderBottomColor: '#ddd',
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#fff',
    padding: 20,
    backgroundColor: '#252525',
    borderTopWidth: 2,
    borderTopColor: '#ededed',
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: '#E91E63',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});

