import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    TextInput, Button, Dimensions
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
// import moduleName from './../images/logo.svg'
//bbjirr fullwidth aja susah https://stackoverflow.com/questions/39631895/how-to-set-image-width-to-be-100-and-height-to-be-auto-in-react-native
const win = Dimensions.get('window');
const ratio = win.width / 541;

import skillData from '../skillData.json'

export const App = (props) => {
    const kategori = ['Framework / Library', 'Bahasa Pemrograman', 'Technology']
    // onPressLearnMore() {
    //     console.log("App -> onPressLearnMore -> onPressLearnMore")
    // }
    console.log("skillData", skillData)
    let flatList = skillData.items.map((val, key) => {
        return (
            <Flat val={val} key={key} />
        )
    })
    let chipList = kategori.map((val, key) => <ChipBar kategori={val} key={key} />)
    return (
        <View style={style.container}>
            <Image source={require('./../images/sanber.png')} style={style.imgLogo} />
            <View style={{ flexDirection: 'row' }}>
                <FontAwesome name="user-secret" size={48} color="#005F89" />
                <View style={{ flexDirection: 'column', alignItems: 'stretch', paddingLeft: 8 }}>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>Hi</Text>
                    <Text style={{ fontSize: 16, marginBottom: 8, fontWeight: "bold" }}>Mikasa Ackerman</Text>
                </View>
            </View>
            <View style={{ flexDirection: "row" }}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                >

                    {chipList}
                </ScrollView>
            </View>
            <View style={{ flexDirection: "column" ,paddingTop:16,paddingBottom:32}}>
                <ScrollView
                >
                    {flatList}
                </ScrollView>
            </View>

        </View>
    )

}

export default App

export const ChipBar = (props) => {
    return (
        <View style={{ padding: 8, marginHorizontal: 4, backgroundColor: "#E6ECEB" }}>
            <Text style={{ fontSize: 16 }}>{props.kategori}</Text>
        </View>
    )
}

export const Flat = (props) => {
    return (
        <View style={{
            background: 'white', marginBottom: 8, borderRadius: 8, padding: 16,

            //https://ethercreative.github.io/react-native-shadow-generator/
        }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <MaterialCommunityIcons name={props.val.iconName} size={64} color="#005F89" />
                <View style={{ flexDirection: 'column', alignItems: 'stretch', paddingLeft: 16 }}>
                    <Text style={{ fontSize: 24, fontWeight: 'bold', color: "#005F89" }}>{props.val.skillName}</Text>
                    <Text style={{ fontSize: 12, color: "#005F89" }}>{props.val.categoryName}</Text>
                    <Text style={{ fontSize: 32, fontWeight: 'bold' }}>{props.val.percentageProgress}</Text>
                </View>
                <View>

                </View>
            </View>

        </View>
    )
}


const style = StyleSheet.create({
    container: {
        padding: 16,
        // backgroundColor: 'gray',
        flex: 1,
        flexDirection: 'column',
        // flexDirection: 'row',
    },
    imgLogo: {
        // position: 'absolute',
        // flex:'start',
        // width: 165,
        height: 40,
        marginLeft: -20,
        marginTop: 26,
        marginBottom: 35,
        // left: 14
    },
    textInput: {
        height: 40, borderColor: 'gray', borderWidth: 1, paddingHorizontal: 8,
        marginBottom: 16,
        width: 273,
    },
    buttonSubmitView: {
        marginTop: 124,
        // width: 143,
        // flex: 1,

        flexDirection: 'row-reverse',
    },
    touchButtonSubmit: {
        height: 100, width: 143,
        borderRadius: 8
    },
    imgProfil: {
        width: win.width - 16 * 2,
        height: 362 * ratio,
        marginBottom: 64
    },
    textWithLogo: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        fontSize: 16,
        color: 'black'
    }
})
