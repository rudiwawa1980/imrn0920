import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import React, { useState, useEffect } from 'react'
// Buatlah sebuah folder “TugasNavigation” di dalam folder “Tugas” masukkan LoginScreen.js, AboutScreen.js, dan SkillScreen.js.
import AboutScreen from '../../tugas13/Components/AboutScreen'
import SkillScreen from '../../tugas14/components/SkillScreen'
import LoginScreen from '../../tugas13/Components/LoginScreen'
import { ProjectScreen } from "./ProjectScreen";
import { AddScreen } from "./AddScreen";



const Drawer = createDrawerNavigator();

export const DrawerStackScreen = (params) => {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Profile" component={AboutScreen} />
            <Drawer.Screen name="Home" component={TabsScreen} />
            {/* <Drawer.Screen name="Skill" component={SkillScreen} /> */}
        </Drawer.Navigator>
    )
}
const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="skill" component={SkillScreen} />
        <Tabs.Screen name="project" component={ProjectScreen} />
        <Tabs.Screen name="add" component={AddScreen} />
    </Tabs.Navigator>
);

const Stack = createStackNavigator();
const RootStackNavigation = ({ userToken }) => (
    <Stack.Navigator headerMode="none">
        {userToken ? (
            <Stack.Screen name="Home" component={DrawerStackScreen} />
            ) : (
                <Stack.Screen name="Login" component={LoginScreen} />
                )}
    </Stack.Navigator>
)

export const App = (params) => {
    let userToken = true
    return (
        <NavigationContainer>
            <RootStackNavigation userToken={userToken} />

        </NavigationContainer>
    )
}

export default App