//If-else
	
var nama = "danang"
var peran = "Werewolf"

if(nama=="") console.log('Nama harus diisi!')
else if(nama!=""&&peran=="") console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
else {
  if(peran=="Penyihir") console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
  else if(peran=="Guard") console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
  else if(peran=="Werewolf") console.log("Halo "+peran+" "+nama+",  Kamu akan memakan mangsa setiap malam!")
}


//Switch Case
var hari = 21; 
var bulan = 2; 
var tahun = 1945;
var bulanStr = "";
switch(bulan) {
  case 1:   { bulanStr="Januari"; break; }
  case 2:   { bulanStr="Februari"; break; }
  case 3:   { bulanStr="Maret"; break; }
  case 4:   { bulanStr="April"; break; }
  case 5:   { bulanStr="Mei"; break; }
  case 6:   { bulanStr="Juni"; break; }
  case 7:   { bulanStr="Juli"; break; }
  case 8:   { bulanStr="Agustus"; break; }
  case 9:   { bulanStr="September"; break; }
  case 10:   { bulanStr="Oktober"; break; }
  case 11:   { bulanStr="November"; break; }
  case 12:   { bulanStr="Desember"; break; }
  default:  { console.log('Tidak terjadi apa-apa'); }}
 
console.log(hari+" "+bulanStr+" "+tahun)

