// Release 0
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
}

let sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name) {
        super(name)
    }
    yell() {
        console.log("Auooo")
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        console.log("hop hop")
    }
}
let sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

let kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// 2. Function to Class
class Clock {
    constructor({ template }) {
        this._template = template
        this._timer = 0

    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;


        let template = this._template
        let output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this._timer);
    };

    start() {
        this.render();
        this._timer = setInterval(this.render.bind(this), 1000);
    };



    // Code di sini
}


let clock = new Clock({ template: 'h:m:s' });
clock.start(); 