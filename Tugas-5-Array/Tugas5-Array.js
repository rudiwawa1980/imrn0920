//Soal No. 1 (Range)

function range(startNum, finishNum) {
  var result = []
  if(startNum<finishNum){
    for (var i = startNum; i<=finishNum ; i++){
      result.push(i)
    }
  }
  else if(startNum>finishNum){
    for (var i = startNum; i>=finishNum ; i--){
      result.push(i)
    }
  }
  else{
    return -1
  }
  return result
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log('\n')


// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum,step) {
  var result = []
  if(startNum<finishNum){
    for (var i = startNum; i<=finishNum ; i+=step){
      result.push(i)
    }
  }
  else if(startNum>finishNum){
    for (var i = startNum; i>=finishNum ; i-=step){
      result.push(i)
    }
  }
  else{
    return -1
  }
  return result
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log('\n')


// Soal No. 3 (Sum of Range)

function sum(startNum, finishNum,step){
  var deret=[]
  result = 0
  if(step){
    deret=rangeWithStep(startNum, finishNum,step)
  }else if(startNum&&finishNum){
    deret=rangeWithStep(startNum, finishNum,1)
  }else if(startNum) return startNum
  else return 0
  for (i = 0; i < deret.length; i++) {
    result += deret[i];
  }
  return result
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("\n") // 0 


//Soal No. 4 (Array Multidimensi)
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
  
function dataHandling (params){
  for (i = 0; i < params.length; i++) {
      console.log("Nomor ID: "+params[i][0])
      console.log("Nama Lengkap:  "+params[i][1])
      console.log("TTL:  "+params[i][2]+" "+params[i][3])
      console.log("Hobi:  "+params[i][4]+"\n")
  }
}

dataHandling(input)


// Soal No. 5 (Balik Kata)

function balikKata(kata) {
  var result=""
  for (i = kata.length-1 ; i >= 0; i--) {
    result +=kata.charAt(i)
  }
  return result
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("\n")


// Soal No. 6 (Metode Array)

// ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
function dataHandling2(input){
input.splice(1, 1, input[1]+" Elsharawy") 
input.splice(2, 1, "Provinsi "+input[2]) 
input.splice(4, 1, "Pria") 
input.splice(5, 0, "SMA Internasional Metro") 
console.log(input)

var arrTglLahir = input[3].split('/')
var bulanNumber = parseInt(arrTglLahir[1] ) 
var bulan
switch(bulanNumber){
  case 1: bulan = "januari";break;
  case 2: bulan = "februari";break;
  case 3: bulan = "maret";break;
  case 4: bulan = "april";break;
  case 5: bulan = "mei";break;
  case 6: bulan = "juni";break;
  case 7: bulan = "juli";break;
  case 8: bulan = "agustus";break;
  case 9: bulan = "september";break;
  case 10: bulan = "oktober";break;
  case 11: bulan = "nopember";break;
  case 12: bulan = "desember";break;
}
var arrTglLahirJoin = arrTglLahir.join("-")
console.log(bulan)
console.log(arrTglLahir.sort(function (value1,value2) {return value2-value1}))
console.log(arrTglLahirJoin)
console.log(input[1].slice(0,15))
}

input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
dataHandling2(input);
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
